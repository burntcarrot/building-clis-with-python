import click
from utils.database import wipe_table


@click.command()
@click.pass_context
def wipe(ctx):
    conn = ctx.obj["conn"]
    wipe_table(conn)
    print("Deleted all notes.")
