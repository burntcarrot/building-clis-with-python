import click


@click.command()
@click.pass_context
def show(ctx):
    cursor = ctx.obj["cursor"]
    cursor.execute("SELECT * FROM notes;")
    rows = cursor.fetchall()

    console = ctx.obj["console"]
    table = ctx.obj["table"]
    for row in rows:
        id, title, content, date = row
        table.add_row(str(id), title, content, date)

    console.print(table)
