from click.testing import CliRunner
from commands.wipe import wipe
from utils.database import create_connection

def test_wipe_command(monkeypatch):
    runner = CliRunner()
    monkeypatch.setenv("DB_PATH", ":memory:")
    conn, c = create_connection()

    result = runner.invoke(wipe, obj={"conn": conn})

    assert result.exit_code == 0
    assert 'Deleted all notes.' in result.output

    c.execute("SELECT * FROM notes")
    notes = c.fetchall()

    assert len(notes) == 0
