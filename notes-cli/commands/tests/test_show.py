from click.testing import CliRunner
from commands.show import show
from utils.database import create_connection
from rich.console import Console
from rich.table import Table

def test_show_command(monkeypatch):
    runner = CliRunner()
    monkeypatch.setenv("DB_PATH", ":memory:")
    conn, c = create_connection()

    c.execute("INSERT INTO notes (title, content) VALUES (?,?)", ("test title 1", "test content 1"))
    c.execute("INSERT INTO notes (title, content) VALUES (?,?)", ("test title 2", "test content 2"))
    conn.commit()

    console = Console()
    table = Table(title="Notes")
    table.add_column("ID")
    table.add_column("Title")
    table.add_column("Content")
    table.add_column("Date")

    result = runner.invoke(show, obj={"cursor": c, "console": console, "table": table})
    assert result.exit_code == 0
    assert 'test title 1' in result.output
    assert 'test title 2' in result.output
