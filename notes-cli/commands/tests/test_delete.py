from click.testing import CliRunner
from commands.delete import delete
from utils.database import create_connection

def test_add_command(monkeypatch):
    runner = CliRunner()
    monkeypatch.setenv("DB_PATH", ":memory:")
    conn, c = create_connection()

    c.execute("INSERT INTO notes (title, content) VALUES (?,?)", ("test title 1", "test content 1"))
    c.execute("INSERT INTO notes (title, content) VALUES (?,?)", ("test title 2", "test content 2"))
    conn.commit()

    result = runner.invoke(delete, ["1"], obj={"conn": conn, "cursor": c})
    assert result.exit_code == 0

    c.execute("SELECT * FROM notes")
    rows = c.fetchall()

    assert len(rows) == 1
    assert rows[0][0] == 2
