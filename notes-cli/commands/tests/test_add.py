from click.testing import CliRunner
from commands.add import add
from utils.database import create_connection

def test_add_command(monkeypatch):
    runner = CliRunner()
    monkeypatch.setenv("DB_PATH", ":memory:")
    conn, c = create_connection()

    result = runner.invoke(add, ["test title", "test content"], obj={"conn": conn, "cursor": c})

    assert result.exit_code == 0
    assert 'Added a new note (ID = 1) with title: "test title"' in result.output

    c.execute("SELECT * FROM notes WHERE id=?", (1,))
    note = c.fetchone()

    assert note is not None
    assert note[1] == "test title"
    assert note[2] == "test content"
