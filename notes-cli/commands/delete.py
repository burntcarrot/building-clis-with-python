import click


@click.command()
@click.argument("id", type=int)
@click.pass_context
def delete(ctx, id):
    conn = ctx.obj["conn"]
    cursor = ctx.obj["cursor"]

    cursor.execute("DELETE FROM notes WHERE id=?", (id,))
    conn.commit()
