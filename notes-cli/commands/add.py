import click


@click.command()
@click.argument("title")
@click.argument("content")
@click.pass_context
def add(ctx, title, content):
    conn = ctx.obj["conn"]
    cursor = ctx.obj["cursor"]

    cursor.execute(
        "INSERT INTO notes (title, content) VALUES (?,?) RETURNING id;",
        (title, content),
    )
    rows = cursor.fetchall()
    note_id = rows[0][0]
    print(f'Added a new note (ID = {note_id}) with title: "{title}"')
    conn.commit()
