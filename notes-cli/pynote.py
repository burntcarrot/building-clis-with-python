import click
from commands.add import add
from commands.show import show
from commands.delete import delete
from commands.wipe import wipe
from utils.database import create_connection
from utils.ui import setup_ui


@click.group
@click.pass_context
def cli(ctx):
    ctx.ensure_object(dict)
    conn, cursor = create_connection()
    ctx.obj["conn"] = conn
    ctx.obj["cursor"] = cursor

    console, table = setup_ui()
    ctx.obj["console"] = console
    ctx.obj["table"] = table


cli.add_command(add)
cli.add_command(show)
cli.add_command(delete)
cli.add_command(wipe)

if __name__ == "__main__":
    cli()
