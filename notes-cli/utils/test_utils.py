from utils.ui import setup_ui
from rich.table import Table
from rich.console import Console

def test_setup_ui():
    console, table = setup_ui()
    assert isinstance(console, Console)
    assert isinstance(table, Table)
    assert len(table.columns) == 4
    assert table.columns[0].header == "ID"
    assert table.columns[1].header == "Title"
    assert table.columns[2].header == "Content"
    assert table.columns[3].header == "Date"
    assert table.show_header is True
    assert table.header_style == "bold magenta"
