import sqlite3
from pathlib import Path
import os


HOME_PATH = Path.home()

def create_connection():
    try:
        DB_PATH = os.environ.get("DB_PATH") or (HOME_PATH / "notes.db")
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute(
            f"CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT NOT NULL, content TEXT NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP )"
        )
    except sqlite3.Error as e:
        raise e

    return conn, cur


def wipe_table(conn):
    conn.execute("DELETE FROM notes")
    conn.commit()
