from rich.console import Console
from rich.table import Table


def setup_ui():
    console = Console()

    table = Table(show_header=True, header_style="bold magenta")
    table.add_column("ID")
    table.add_column("Title")
    table.add_column("Content")
    table.add_column("Date")

    return console, table
