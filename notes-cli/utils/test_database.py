from utils.database import create_connection
import sqlite3

def test_create_connection(monkeypatch):
    monkeypatch.setenv("DB_PATH", ":memory:")
    conn, cursor = create_connection()

    assert isinstance(conn, sqlite3.Connection)
    assert isinstance(cursor, sqlite3.Cursor)
