import nox


@nox.session(python=["3.10"])
def test(session):
    session.install("-r", "requirements.txt")
    session.run("pytest")
