from setuptools import setup, find_packages

setup(
    name='pynote',
    author='Your Name',
    author_email='your_email@example.com',
    url='https://github.com/yourusername/yourproject',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        line.strip() for line in open('requirements.txt')
    ],
    entry_points='''
        [console_scripts]
        pynote=pynote:cli
    '''
)
