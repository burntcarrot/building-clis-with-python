import click

@click.command()
@click.argument('filenames', nargs=-1)
def cat(filenames):
    for filename in filenames:
        with open(filename) as f:
            click.echo(f"======== {filename} ========")
            click.echo(f.read())

if __name__ == '__main__':
    cat()
