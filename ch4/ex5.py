import click

@click.command()
@click.argument('values', nargs=-1, type=int)
def add(values):
    result = 0
    for value in values:
        result += value
    click.echo(result)

if __name__ == '__main__':
    add()
