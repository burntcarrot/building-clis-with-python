import click

@click.command()
@click.option('--color/--no-color', default=False)
def greet(color):
    if color:
        text = click.style('Hello, world!', fg="green")
    else:
        text = 'Hello, world!'
    click.echo(text)

if __name__ == '__main__':
    greet()
