import os
import click
import tarfile

@click.group()
def tar():
    pass

@tar.command()
@click.argument('filename', type=click.Path())
@click.argument('files', nargs=-1, type=click.Path(exists=True))
def create(filename, files):
    try:
        with tarfile.open(filename, 'w:gz') as tar:
            for f in files:
                if os.path.isdir(f):
                    for root, dirs, subfiles in os.walk(f):
                        for subfile in subfiles:
                            file_path = os.path.join(root, subfile)
                            tar.add(file_path)
                        for subdir in dirs:
                            dir_path = os.path.join(root, subdir)
                            tar.add(dir_path)
                else:
                    tar.add(f)
            click.echo(f'Tar archive created: {filename}')
    except Exception as e:
        click.echo(f'Error creating tar archive: {str(e)}')

@tar.command()
@click.argument('filename', type=click.Path(exists=True))
@click.argument('output_path', default='.')
def extract(filename, output_path):
    try:
        with tarfile.open(filename, 'r:gz') as tar:
            tar.extractall(output_path)
            click.echo(f'Tar archive extracted to: {output_path}')
    except Exception as e:
        click.echo(f'Error extracting tar archive: {str(e)}')

if __name__ == '__main__':
    tar()
