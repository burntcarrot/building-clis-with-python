import click

@click.command()
@click.argument('filename')
def cat(filename):
    try:
        with open(filename) as f:
            click.echo(f.read())
    except FileNotFoundError:
        click.echo(f'Error: file "{filename}" not found', err=True)
    except PermissionError:
        click.echo(f'Error: permission denied for file "{filename}"', err=True)

if __name__ == '__main__':
    cat()
