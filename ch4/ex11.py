import click
import os
from click_aliases import ClickAliasedGroup

@click.group(cls=ClickAliasedGroup)
def cli():
    pass

@cli.command('ls', aliases=['list', 'l'])
@click.option('-R', '--recursive', is_flag=True, help='Recursively list subdirectories.')
@click.argument('directory', default='.', type=click.Path(exists=True))
def ls(recursive, directory):
    """List directory contents."""
    if recursive:
        for root, dirs, files in os.walk(directory):
            click.echo(f"{click.style(root, fg='blue')}:")
            for name in sorted(dirs + files):
                path = os.path.join(root, name)
                if os.path.isdir(path):
                    name = click.style(name, fg='blue')
                click.echo(name)
    else:
        for name in os.listdir(directory):
            path = os.path.join(directory, name)
            if os.path.isdir(path):
                name = click.style(name, fg='blue')
            click.echo(name)

if __name__ == "__main__":
    cli()
