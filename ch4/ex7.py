import random
import click

@click.command()
def get_lucky():
    if click.confirm('Do you want to get lucky?'):
        if random.randint(0, 1) == 0:
            click.echo('Sorry, better luck next time.')
        else:
            click.echo('Congratulations, you got lucky!')
    else:
        click.echo('Maybe next time.')

if __name__ == '__main__':
    get_lucky()
