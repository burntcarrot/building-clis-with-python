import click

@click.command()
@click.argument('filename')
def cat(filename):
    with open(filename) as f:
        click.echo(f.read())

if __name__ == '__main__':
    cat()
