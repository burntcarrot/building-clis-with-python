import argparse


def read_lines(file, lines):
    if lines <= 0:
        print("Invalid input: number of lines should be greater than 0.")
        return

    try:
        with open(file) as f:
            for i, line in enumerate(f):
                if i == lines:
                    break
                print(line, end='')
    except FileNotFoundError:
        print(f"Error: The file '{file}' could not be found.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read the first X lines from a file')
    parser.add_argument('file', type=str, help='the file to read')
    parser.add_argument('-n', '--lines', type=int, default=10, help='the number of lines to read')
    args = parser.parse_args()

    read_lines(args.file, args.lines)
