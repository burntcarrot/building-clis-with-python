# Building CLIs in Python

Code for the mon.school course "Building CLIs in Python".

## Getting started

Clone this repository and change your directory to the cloned repository.

Follow the instructions as described in the course.

## Components

The repository contains three directories:

- `ch3`: Contains code for Chapter 3 (Building CLIs using argparse)
- `ch4`: Contains code for Chapter 4 (Building CLIs using click)
- `notes-cli`: Contains code for `pynote`, our note-taking CLI (Chapter 5 & 6)
